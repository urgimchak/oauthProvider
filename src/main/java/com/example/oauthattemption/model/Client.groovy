package com.example.oauthattemption.model

import org.codehaus.jackson.annotate.JsonIgnore

import javax.persistence.*;

@Entity
@Table(name="oauth_client_details")
public class Client {

    @Id
    @Column(name="client_id")
    private String client

    @Column
    private String resource_ids

    @Column
    @JsonIgnore
    private String client_secret

    @Column
    private String scope

    @Column
    private String authorized_grant_types

    @Column
    private String web_server_redirect_uri

    @Column
    private String authorities

    @Column
    private int access_token_validity

    @Column
    private int refresh_token_validity

    @Column
    private String additional_information

    @Column
    private String autoapprove

    @Column
    //@JsonIgnore
    private String password

    void setPassword(String password) {
        this.password = password
    }

    String getPassword() {

        return password
    }

    void setClient(String client) {
        this.client = client
    }

    void setResource_ids(String resource_ids) {
        this.resource_ids = resource_ids
    }

    void setClient_secret(String client_secret) {
        this.client_secret = client_secret
    }

    void setScope(String scope) {
        this.scope = scope
    }

    void setAuthorized_grant_types(String authorized_grant_types) {
        this.authorized_grant_types = authorized_grant_types
    }

    void setWeb_server_redirect_uri(String web_server_redirect_uri) {
        this.web_server_redirect_uri = web_server_redirect_uri
    }

    void setAuthorities(String authorities) {
        this.authorities = authorities
    }

    void setAccess_token_validity(int access_token_validity) {
        this.access_token_validity = access_token_validity
    }

    void setRefresh_token_validity(int refresh_token_validity) {
        this.refresh_token_validity = refresh_token_validity
    }

    void setAdditional_information(String additional_information) {
        this.additional_information = additional_information
    }

    void setAutoapprove(String autoapprove) {
        this.autoapprove = autoapprove
    }

    String getClient() {

        return client
    }

    List<String> getResource_ids() {

        List<String> resourceList = new ArrayList<String>(Arrays.asList(resource_ids.split(",")))
        return resourceList
    }

    String getClient_secret() {
        return client_secret
    }

    List<String> getScope() {
        List<String> scopeList = new ArrayList<String>(Arrays.asList(scope.split(",")))
        return scopeList
    }

    List<String> getAuthorized_grant_types() {
        List<String> authList = new ArrayList<String>(Arrays.asList(authorized_grant_types.split(",")))
        return authList
    }

    String getWeb_server_redirect_uri() {
        return web_server_redirect_uri
    }

    String getAuthorities() {
        return authorities
    }

    int getAccess_token_validity() {
        return access_token_validity
    }

    int getRefresh_token_validity() {
        return refresh_token_validity
    }

    String getAdditional_information() {
        return additional_information
    }

    String getAutoapprove() {
        return autoapprove
    }

}
