package com.example.oauthattemption;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthattemptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthattemptionApplication.class, args);
	}
}
