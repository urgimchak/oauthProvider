package com.example.oauthattemption.repositories;

import com.example.oauthattemption.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
    Client findByClient(String client_id);

}
