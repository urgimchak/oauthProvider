package com.example.oauthattemption.service

import com.example.oauthattemption.model.Client
import com.example.oauthattemption.model.User

public interface ClientService {
    Client save(Client client);
    List<Client> findAll();
    void delete(long id);
}
