//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.oauthattemption.service.impl;

import com.example.oauthattemption.model.Client;
import com.example.oauthattemption.repositories.ClientRepository;
import com.example.oauthattemption.service.ClientService;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("clientService")
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;


    public void delete(long id) {
        //this.clientRepository.delete(clientRepository.findById(id));
    }

    public Client save(Client client) {
        return (Client)this.clientRepository.save(client);
    }

    public List<Client> findAll() {
        List<Client> list = new ArrayList();
        this.clientRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }
}
