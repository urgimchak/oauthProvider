//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.oauthattemption.service.impl;

import com.example.oauthattemption.model.User;
import com.example.oauthattemption.repositories.UserRepository;
import com.example.oauthattemption.service.UserService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
	UserRepository userRepository;

	public UserServiceImpl() {
	}

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		User user = this.userRepository.findByUsername(userId);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		} else {
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), Arrays.asList(new SimpleGrantedAuthority(user.getAuthority())));
		}
	}

	public List<User> findAll() {
		List<User> list = new ArrayList();
		this.userRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	public void delete(long id) {
		//this.userRepository.delete(id);
	}

	public User save(User user) {
		return (User)this.userRepository.save(user);
	}
}
