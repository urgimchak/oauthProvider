package com.example.oauthattemption.service;


import com.example.oauthattemption.model.User

import java.util.List

interface UserService {

    User save(User user)
    List<User> findAll()
    void delete(long id)
}
