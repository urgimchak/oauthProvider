package com.example.oauthattemption.controller

import com.example.oauthattemption.model.Client
import com.example.oauthattemption.model.User
import com.example.oauthattemption.service.ClientService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/clients")
//@Secured('ROLE_ADMIN')
class ClientController {
    
    @Autowired
    ClientService clientService

    @RequestMapping(value="/client", method = RequestMethod.GET)
    public List<Client> listUser(){
        return clientService.findAll()
    }

    @RequestMapping(value = "/client", method = RequestMethod.POST)
    public Client create(@RequestBody Client client){
        client.setClient("newClient")
        return clientService.save(client)
    }

    @RequestMapping(value = "/client/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable(value = "id") Long id){
        clientService.delete(id)
        return "success"
    }
}
