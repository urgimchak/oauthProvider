package com.example.client.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class UserController {

    @Autowired
    private OAuth2RestOperations restTemplate;

    @Value("${security.oauth2.resource.user-info-uri}")
    private String resourceURI;

    @GetMapping("/")
    public String home() {
        System.out.println("HERE!");
        return restTemplate.getForObject(resourceURI, String.class);
    }

}